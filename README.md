# Image and Video Technology - Exercises.
Solution of to the exercises of ivt 2018, course part of the Bruface program. Electrical engineering - MA2.
## Getting Started
Clone the repository and open the specific project-solution in codeblocks. Just for reference the version used was 17.12.
## Authors

* **Evert Ismael Pocoma Copa** - *Master Student* - evertismael@gmail.com
