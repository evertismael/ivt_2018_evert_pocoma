#include <iostream>

using namespace std;

int main()
{
        // generate the values of the image.
    char imageValues[256*256];
    for (int x=0; x<256;x++){
        for (int y=0; y<256;y++){
             imageValues[x +256*y] =char((0.5 + 0.5*cos(x*(M_PI/32))*cos(y*(M_PI/64)))*255);
             //cout<<imageValues[x][y]<<" ";
        }
        //cout<<endl;
    }
    // save the values to a file.raw
    saveFile(imageValues);
    cout<<"All tasks finished successfully"<<endl;
    return 0;
}
