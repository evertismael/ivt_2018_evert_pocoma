#include <iostream>
/* Including my libs */
#include "myLibs.h"

# define M_PI           3.14159265358979323846  /* pi */

using namespace std;
int main()
{
    // generate the values of the image.
    float *img = new float[256*256]();
    for (int x=0; x<256;x++){
        for (int y=0; y<256;y++){
             *(img + x +256*y) = 0.5 + 0.5*cos(x*(M_PI/32.0))*cos(y*(M_PI/64.0));
        }
    }
    s1_saveImg("s1_2_1.raw", img, 256);

    cout << "end of execution" << endl;
    return 0;
}
