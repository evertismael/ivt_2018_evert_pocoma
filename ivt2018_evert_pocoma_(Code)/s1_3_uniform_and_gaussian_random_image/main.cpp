#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    cout << "====== Uniform random noise ======= " << endl;
    float *img_u = new float[256*256]();
    float mse_u = 0.0;

    img_u = s1_addUniformNoise(img_u, 256, 0.0, 1.0);
    mse_u = s1_computeMSE(img_u,256);
    s1_saveImg("s1_3_1.raw", img_u, 256);
    cout << "MSE: "<< mse_u << endl;

    cout << "====== Gaussian random noise ======= " << endl;
    float *img_g = new float[256*256]();
    float mse_g = 0.0;

    img_g = s1_addGaussianNoise(img_g, 256, 0.5, 5.0);
    mse_g = s1_computeMSE(img_g,256);
    s1_saveImg("s1_3_2_std_5.raw", img_g, 256);
    cout << "MSE: "<< mse_g << endl;

    cout << "====== Find the gaussian noise parameters for mse_g == mse_u ======= " << endl;
    float stddev = 0.30;
    do{
        stddev -=0.001;
        img_g = new float[256*256]();
        img_g = s1_addGaussianNoise(img_g, 256, 0.5, stddev);
        mse_g = s1_computeMSE(img_g,256);

    }while(abs(mse_g-mse_u)>0.001);

    s1_saveImg("s1_3_2_mseg_equal_mseu.raw", img_g, 256);
    cout << "MSE uniform: "<< mse_u << endl;
    cout << "MSE gaussian: "<< mse_g << endl;
    cout << "std: "<< stddev << endl;


    cout << "end of execution" << endl;
    return 0;
}
