#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>


#define M_PI   3.14159265358979323846264338327950288
using namespace std;

/* Converts an integer in binary (string)*/
std::string s6_dec2bin(unsigned n){
    std::string res;
    while (n) {
        res.push_back((n & 1) + '0');
        n >>= 1;
    }
    if (res.empty())
        res = "0";
    else
        std::reverse(res.begin(), res.end());
   return res;
}

/* Converts the string binary into an integer*/
unsigned s6_bin2dec(std::string n){
    int N = n.length();
    char msb;
    unsigned number = 0;

    for (int i=0;i<N;i++){
        msb = n.at(i);
        if (msb=='1'){
            number = number*2 + 1;
        }else {
            number = number*2;
        }
    }
   return number;
}

/* returns the respective golomb code for the integer.*/
std::string s6_golomb(unsigned inVal){
    string outVal,x_plus_1,zeros = "";
    int Nbits = 0;
    // 1. Write down x+1 in binary
    x_plus_1 = s6_dec2bin(inVal+1); //to binary
    // 2. Count the bits written, subtract one, and write that number of starting
    //    zero bits preceding the previous bit string.
    Nbits = x_plus_1.length();
    for(int i=0; i<Nbits-1; i++){
        zeros = zeros + "0";
    }
    outVal = zeros + x_plus_1;
    return outVal;
}

/* Returns the integer for the golomb code*/
unsigned s6_golomb(std::string inVal){
    unsigned outVal=0;
    string x_plus_1 = "";
    int Nzeros = inVal.find("1");
    x_plus_1 = inVal.substr(Nzeros);
    outVal = s6_bin2dec(x_plus_1) - 1;
    return outVal;
}

/* Adds an extra layer for golomb with negative numbers*/
std::string s6_golomb_signed(int inValSigned){
    int inVal = abs(inValSigned)*2;
    if (inValSigned > 0){
        inVal = inVal - 1;
    }
    string outString = s6_golomb(unsigned(inVal));
    return outString;
}

/* Adds an extra layer for golomb with negative numbers*/
int s6_golomb_signed(std::string inVal){
    unsigned inNum = s6_golomb(inVal);
    int outNum;
    if(inNum%2 ==0){
        outNum = (inNum/2)*(-1);
    }else {
        outNum = (inNum + 1)/2;
    }
    return outNum;
}

/* Saves the bitStream to a file*/
int s6_saveString2File(const char* fileName, std::string bitStream){
    std::ofstream out(fileName);
    out << bitStream;
    out.close();
    cout<<"compressed file created: "<<fileName<<endl;
    return 0;
}

/* Saves the bitStream to a file*/
std::string s6_readStringFromFile(const char* fileName){
    std::string line, bitStream;
    ifstream infile (fileName);
    while(getline(infile, line)) {
        bitStream = line;
    }
    return bitStream;
}

/* Merges the DC delta and AC RLE coefficients */
float* s6_mergeArrays(float* array1, int dim1, float* array2, int dim2){
    float* outArray = new float[dim1 + dim2]();
    for (int i = 0; i < dim1; i++){
        *(outArray + i) = *(array1 + i);
    }
    for (int i = 0; i < dim2; i++){
        *(outArray + dim1 + i) = *(array2 + i);
    }
    return outArray;
}
/* Separates the DC delta and AC RLE coefficients */
int s6_separateArrays(float* source, float* array1, int dim1, float* array2, int dim2){
    for (int i = 0; i < dim1; i++){
        *(array1 + i) = *(source + i);
    }
    for (int i = 0; i < dim2; i++){
        *(array2 + i) = *(source + dim1 + i);
    }
    return 0;
}

/* Compress the image and returns a bitStream in string format*/
std::string s6_compress(const char* fileName, int dim, bool debug){
    float* img = s2_loadImg(fileName, dim);
    // DC terms.
    float* img_dcEnc = s4_encode(img, 256, false);
    float* img_dcEnc_dc = s5_keepDCterm(img_dcEnc);
    float* delta_enc = s5_deltaEncoding(img_dcEnc_dc, 32);
    // AC terms.
    float* img_acRle = new float[256*256*2]();
    int N_acSymbols = s5_encode(img,256,false,img_acRle, false);

    // Merging in one coefficients stream
    float* numberStream = s6_mergeArrays(delta_enc, 32*32, img_acRle, N_acSymbols*2);
    if (debug){
        cout<<"DC delta terms"<<endl;
        for (int i=0; i<10; i++) cout<<*(numberStream + i)<<" ";
        cout<<"..."<<endl;
        cout<<"AC delta terms"<<endl;
        for (int i=0; i<10; i++) cout<<*(img_acRle + i)<<" ";
        cout<<"..."<<endl;
    }

    int streamLength = 32*32 + N_acSymbols*2;

    // getting stringStream.
    std::string outStream = "";

    float val = 0;
    for (int i=0; i < streamLength; i++){
        val = *(numberStream + i);
        outStream = outStream + s6_golomb_signed(int(val));
    }
    return outStream;
}

/* Decompress a file*/
float* s6_decompress(const char* fileName, int dim, bool debug){
    std::string bitStream = s6_readStringFromFile(fileName);
    // from bitStream to numStream.
    float* numStream = new float[dim*dim]();
    int i = 0;
    while(bitStream.length()>0){
        int Nbits = bitStream.find("1");
        std::string golombString = bitStream.substr(0,2*Nbits+1);
        float val = (float)s6_golomb_signed(golombString);
        *(numStream + i) = val;
        i++;
        bitStream = bitStream.substr(2*Nbits+1);
    }

    // separate DC from AC terms.
    float* img_dc_deltaEnc = new float[32*32]();
    float* img_acEnc = new float[dim*dim]();
    s6_separateArrays(numStream, img_dc_deltaEnc, 32*32, img_acEnc, i-32*32);
    if (debug){
        cout<<"DC delta terms"<<endl;
        for (int i=0; i<10; i++) cout<<*(img_dc_deltaEnc + i)<<" ";
        cout<<"..."<<endl;
        cout<<"AC rle terms"<<endl;
        for (int i=0; i<10; i++) cout<<*(img_acEnc + i)<<" ";
        cout<<"..."<<endl;
    }
    float* img_dcEnc = s5_deltaDecoding(img_dc_deltaEnc, 32);
    float* img = s5_decode(img_acEnc, img_dcEnc, 256, false, false);
    return img;
}
