#include <iostream>
#include <sstream> // stringstream

/* Including my libs */
#include "myLibs.h"
using namespace std;

int main()
{
    float* img;
    float* img_noised;
    float sigma = 0.0;
    string outputName = "";

    img = s2_loadImg("lena_256x256.raw",256);
    for (int i=0; i < 80 ; i = i + 20){
        sigma = i;
        img_noised = s1_addGaussianNoise(img, 256, 0.0, sigma);
        outputName = concatStrNumStr("lena_noised_sigma_0", sigma, ".raw");
        s1_saveImg(outputName.c_str(), img_noised, 256);
    }

    cout<<"======== PSNR between two images ========="<<endl;
    img = s2_loadImg("lena_256x256.raw",256);
    img_noised = s2_loadImg("lena_noised_sigma_020.raw",256);
    float psnr = s2_computePsnr(img,img_noised,256,256);
    cout<<"psnr : "<<psnr<<endl;

    img = s2_loadImg("lena_256x256.raw",256);
    img_noised = s2_loadImg("lena_noised_sigma_060.raw",256);
    psnr = s2_computePsnr(img,img_noised,256,256);
    cout<<"psnr : "<<psnr<<endl;

    cout << "end of execution" << endl;
    return 0;
}
