#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#define M_PI   3.14159265358979323846264338327950288
using namespace std;

/* Quantization matrix*/
float * s4_quantizationMatrix(){
    float *q = new float[8*8]();
    float values[] = {16,11,10,16,24,40,51,61,
                      12,12,14,19,26,58,60,55,
                      14,13,16,24,40,57,69,56,
                      14,17,22,29,51,87,80,62,
                      18,22,37,56,68,109,103,77,
                      24,35,55,64,81,104,113,92,
                      49,64,78,87,103,121,120,101,
                      72,92,95,98,112,100,103,99};

    for (int i=0; i<8*8; i++){
        *(q + i) = (float)values[i];
    }
    return q;
}

/* Get the block 8x8 given the indexes x,y (block coordinates)
    block coordinates are given from 0 to N-1
*/

float* s4_getBlock(float* img, int block_x, int block_y){
    float *block = new float[8*8]();
    int index_x = 0, index_y = 0;
    // extract the block.
    for (int y=0; y<8; y++){
        for(int x = 0; x<8; x++){
            // indexes:
            index_x = (block_x)*8 + x;
            index_y = (block_y)*32*8*8 + y*32*8;
            *(block + x + y*8) = *(img + index_x + index_y);
        }
    }
    return block;
}

/*  Saves the value of the block 8x8 given the indexes x,y.
    The values are saved in the global image
    Block coordinates are given from 0 to N-1.
    NOTICE: it returns a pointers but it is just the same pointer
            to the original image and not a copy.
*/

float* s4_saveBlock(float* img, float* block, int block_x, int block_y){
    int index_x = 0, index_y = 0;
    // extract the block.
    for (int y=0; y<8; y++){
        for(int x = 0; x<8; x++){
            // indexes:
            index_x = (block_x)*8 + x;
            index_y = (block_y)*32*8*8 + y*32*8;
            *(img + index_x + index_y) = *(block + x + y*8);
        }
    }
    return img;
}

/* Apply the quantization process in a block of 8x8*/
float* s4_quantize(float* block, float *q, bool clip8bpp){
    float * block_q = new float[8*8];
    float ykl=0.0,qkl =0.0, value = 0.0;
    for (int i = 0; i<8*8 ; i++){
        ykl = *(block +i);
        qkl = *(q +i);
        value = round((ykl)/qkl);
        if (clip8bpp){
            if (value > 127) value = 127;
            if (value < -128) value = -128;
        }
        *(block_q + i) = value;
    }
    return block_q;
}
/* Reverts the quantization*/
float* s4_inverseQuantize(float* block, float *q){
    float * block_iq = new float[8*8];
    float ykl=0,qkl =0;
    for (int i = 0; i<8*8 ; i++){
        ykl = *(block +i);
        qkl = *(q +i);
        *(block_iq + i) = ykl*qkl;
    }
    return block_iq;
}
/* GrayScale 8bpp image.*/
float* s4_grayscale8bpp(float* img, int dim){
    float* img_8bpp = new float[dim*dim]();
    float value = 0.0;
    for (int i = 0; i<dim*dim; i++){
        value = round(*(img+i));
        if (value > 255) value = 255;
        if (value < 0) value = 0;
        *(img_8bpp + i) = value;
    }
    return img_8bpp;
}

/* Encode , DCT, Q*/
float* s4_encode(float* img, int dim, bool saveMiddleSteps){
    float* img_dct = new float[dim*dim]();
    float* img_dct_q = new float[dim*dim]();

    float* dct_basis = s3_getDctBasis(8);
    float* q_matrix = s4_quantizationMatrix();
    // image is 256x256 so 32x32 blocks.
    float* block = new float[8*8]();
    float* block_dct = new float[8*8]();
    float* block_dct_q = new float[8*8]();

    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block = s4_getBlock(img, block_x, block_y);
            block_dct = s3_DCT_transform(block, dct_basis,8, false);
            block_dct_q = s4_quantize(block_dct, q_matrix, false);

            if (saveMiddleSteps){
                img_dct = s4_saveBlock(img_dct, block_dct, block_x, block_y);
            }
            img_dct_q = s4_saveBlock(img_dct_q, block_dct_q, block_x, block_y);
        }
    }

    if (saveMiddleSteps){
        s1_saveImg("enc_dct.raw", img_dct,256);
        s1_saveImg("enc_dct_q.raw", img_dct_q,256);
    }
    return img_dct_q;
}

/* Decode , IQ, IDCT*/
float* s4_decode(float* img_enc, int dim, bool saveMiddleSteps){
    float* dec_iq = new float[dim*dim]();
    float* dec_iq_idct = new float[dim*dim]();

    float* dct_basis = s3_getDctBasis(8);
    float* idct_basis = s3_transpose(dct_basis, 8);

    float* q_matrix = s4_quantizationMatrix();
    // image is 256x256 so 32x32 blocks.
    float* block_enc = new float[8*8]();
    float* block_iq = new float[8*8]();
    float* block_iq_idct = new float[8*8]();

    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block_enc = s4_getBlock(img_enc, block_x, block_y);
            block_iq = s4_inverseQuantize(block_enc, q_matrix);
            block_iq_idct = s3_DCT_transform(block_iq, idct_basis,8, false);

            if (saveMiddleSteps){
                dec_iq = s4_saveBlock(dec_iq, block_iq, block_x, block_y);
            }
            dec_iq_idct = s4_saveBlock(dec_iq_idct, block_iq_idct, block_x, block_y);
        }
    }

    if (saveMiddleSteps){
        s1_saveImg("dec_iq.raw", dec_iq,256);
        s1_saveImg("dec_iq_idct.raw", dec_iq_idct,256);
    }
    return dec_iq_idct;
}
