#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

float* sharp(float *img, int dim, float alpha){
    float *imgOut = new float[dim*dim]();

    float* kernel = s2_getBlurGaussianKernel();
    float* blurredImg = convImgWithKernel(kernel, img, 256, true);

    float sharp = 0.0;
    for (int i=0; i < dim*dim ; i++){
        sharp = *(img + i) - *(blurredImg + i);
        *(imgOut + i) = *(img + i) + alpha * sharp;
    }
    return imgOut;
}

int main()
{
    float* kernel = s2_getBlurGaussianKernel();
    s1_saveImg("blur_gaussian_kernel.raw", kernel, 3);

    float* img = s2_loadImg("lena_256x256.raw", 256);
    float* img_blur_nb = convImgWithKernel(kernel, img, 256, false);
    float* img_blur_wb = convImgWithKernel(kernel, img, 256, true);
    s1_saveImg("img_blur_no_borders.raw", img_blur_nb, 256);
    s1_saveImg("img_blur_with_borders.raw", img_blur_wb, 256);

    cout << "========== psnr of blurred image =============== "<< endl;
    img = s2_loadImg("lena_256x256.raw", 256);
    img_blur_wb = s2_loadImg("img_blur_with_borders.raw", 256);
    float psnr = s2_computePsnr(img, img_blur_wb, 256, 256);
    cout << "psnr: " << psnr << endl;

    cout << "============= unsharp masking  ================= "<< endl;
    float*img_unsharp = sharp(img_blur_wb, 256, 1);
    s1_saveImg("img_unsharp_alpha_1.raw", img_unsharp, 256);
    psnr = s2_computePsnr(img, img_unsharp, 256, 256);
    cout << "psnr: " << psnr << endl;

    cout << "end of execution" << endl;
    return 0;
}
