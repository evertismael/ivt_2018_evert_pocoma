#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
using namespace std;


/*  Saves the values from a memory space to an image file image.raw
    of dimension dim X dim. Returns 0 if everything went well.
    */
int s1_saveImg(const char* fileName, float *img, int dim){
    ofstream image(fileName,std::ios::out | std::ios::binary);
    int result = -1;
    if(!image){
        cout << "****** file not created ********" << endl;
    } else {
        image.write((char*)img,dim*dim*sizeof(float));
        cout <<"file: "<<fileName<<" created"<<endl;
        result = 0;
    }
    image.close();
    return result;
}

/*  Adds uniform random noise to a given image.
    Usual values for min_val is 0 and max_val is 1
    */
float* s1_addUniformNoise(float *img, int dim, float min_val, float max_val){
    float* outImg = new float[dim*dim]();
    float value = 0.0;
    default_random_engine generator;
    uniform_real_distribution<double> distribution(min_val,max_val);

    for (int x=0; x<256;x++){
        for (int y=0; y<256;y++){
             value = distribution(generator);
             *(outImg + x +256*y) = value + *(img + x +256*y);
        }
    }
    return outImg;
}

/*  Adds gaussian random noise to a given image.
    Usual values for min_val is 0 and max_val is 1
    */
float* s1_addGaussianNoise(float *img, int dim, float mean, float stddev){
    float* outImg = new float[dim*dim]();
    float value = 0.0;
    default_random_engine generator;
    normal_distribution<double> distribution(mean,stddev);

    for (int x=0; x<256;x++){
        for (int y=0; y<256;y++){
             value = distribution(generator);
             *(outImg + x +256*y) = value + *(img + x +256*y);
        }
    }
    return outImg;
}


/*  Computes the MSE of a given image
    */
float s1_computeMSE(float *img, int dim){
    float mse = 0.0, value = 0.0;
    for (int x=0; x<256;x++){
        for (int y=0; y<256;y++){
             value = *(img + x +256*y);
             mse+=(value-0.5)*(value-0.5);
        }
    }
    return mse/(dim*dim);
}
