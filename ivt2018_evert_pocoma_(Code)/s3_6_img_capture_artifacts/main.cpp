#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    float* img = s2_loadImg("lena_256x256.raw", 256);
    float* kernel = s2_getBlurGaussianKernel();
    float* img_b = convImgWithKernel(kernel, img, 256, true);
    float psnr_b = s2_computePsnr(img, img_b, 256, 256);

    float sigma = 5.0, psnr_gn = 0.0;
    float* img_gn;
    do{
        sigma += 0.01;
        img_gn = s1_addGaussianNoise(img, 256, 0.0, sigma);
        psnr_gn = s2_computePsnr(img, img_gn, 256, 256);
    }while (abs(psnr_b - psnr_gn)>0.1);

    cout<<"====== sigma for same psnr  ===== "<<endl;
    s1_saveImg("img_blurred.raw", img_b, 256);
    s1_saveImg("img_gaussian.raw", img_gn, 256);
    cout<<" sigma: "<<sigma<<endl;
    cout<<" psnr_gn: "<<psnr_gn<<endl;
    cout<<" psnr_b: "<<psnr_b<<endl;

    cout<<"====== first blur and then gaussian noise ===== "<<endl;
    float* img_1b_2gn = s1_addGaussianNoise(img_b, 256, 0.0, sigma);
    s1_saveImg("img_1b_2gn.raw", img_1b_2gn, 256);
    float psnr_1b_2gn = s2_computePsnr(img, img_1b_2gn,256, 256);
    cout<<" psnr: "<<psnr_1b_2gn<<endl;

    cout<<"====== first gaussian noise and then blur ===== "<<endl;
    float* img_1gn_2b = convImgWithKernel(kernel, img_gn, 256, true);
    s1_saveImg("img_1gn_2b.raw", img_1gn_2b, 256);
    float psnr_1gn_2b = s2_computePsnr(img, img_1gn_2b, 256, 256);
    cout<<" psnr: "<<psnr_1gn_2b<<endl;

    cout << "end of execution" << endl;
    return 0;
}
