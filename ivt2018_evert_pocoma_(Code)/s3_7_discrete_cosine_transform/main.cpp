#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    cout <<" ====== Computing dct dictionaries ====== "<<endl;
    float *dct_basis = s3_getDctBasis(256);
    s1_saveImg("dct_dict.raw", dct_basis,256);

    float * idct_basis = s3_transpose(dct_basis,256);
    s1_saveImg("idct_dict.raw", idct_basis,256);

    float * identity = s3_Mat_multiply(dct_basis, 256, 256, idct_basis, 256, 256);
    s1_saveImg("identity.raw", identity,256);

    cout <<" ====== Performing DCT transform and thresholding ====== "<<endl;

    float* img = s2_loadImg("lena_256x256.raw", 256);
    float* img_dct = s3_DCT_transform(img, dct_basis,256,true);
    s1_saveImg("img_dct.raw", img_dct,256);

    float * img_dct_thr = s3_threshold(img_dct,30.0, 256);
    s1_saveImg("img_dct_thr30.raw", img_dct_thr,256);

    float* img_dct_thr_idct = s3_DCT_transform(img_dct_thr, idct_basis,256,false);
    s1_saveImg("img_dct_thr30_idct.raw", img_dct_thr_idct,256);


    cout <<" ====== Trying several threshold values ====== "<<endl;

    float psnr = 0.0;
    for (int thr=10; thr < 150 ; thr = thr + 40){
        img_dct_thr = s3_threshold(img_dct,float(thr),256) ;
        img_dct_thr_idct = s3_DCT_transform(img_dct_thr, idct_basis,256,false);

        string fileName = concatStrNumStr("img_thr_", float(thr),".raw");
        psnr = s2_computePsnr(img, img_dct_thr_idct,256,256);
        s1_saveImg(fileName.c_str(), img_dct_thr_idct,256);

        cout<<" psnr: "<< psnr << endl;
    }

    cout << "end of execution" << endl;
    return 0;
}
