#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

float* approximate(float* img, int dim, bool saveMiddleSteps){
    float* img_dct = new float[dim*dim]();
    float* img_dct_q = new float[dim*dim]();
    float* img_dct_q_iq = new float[dim*dim]();
    float* img_dct_q_iq_idct = new float[dim*dim]();

    float* dct_basis = s3_getDctBasis(8);
    float* idct_basis = s3_transpose(dct_basis, 8);

    float* q_matrix = s4_quantizationMatrix();
    // image is 256x256 so 32x32 blocks.
    float* block = new float[8*8]();
    float* block_dct = new float[8*8]();
    float* block_dct_q = new float[8*8]();
    float* block_dct_q_iq = new float[8*8]();
    float* block_dct_q_iq_idct = new float[8*8]();

    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block = s4_getBlock(img, block_x, block_y);
            block_dct = s3_DCT_transform(block, dct_basis,8, false);
            block_dct_q = s4_quantize(block_dct, q_matrix, false);
            block_dct_q_iq = s4_inverseQuantize(block_dct_q, q_matrix);
            block_dct_q_iq_idct = s3_DCT_transform(block_dct_q_iq, idct_basis,8, false);

            if (saveMiddleSteps){
                img_dct = s4_saveBlock(img_dct, block_dct, block_x, block_y);
                img_dct_q = s4_saveBlock(img_dct_q, block_dct_q, block_x, block_y);
                img_dct_q_iq = s4_saveBlock(img_dct_q_iq, block_dct_q_iq, block_x, block_y);
            }
            img_dct_q_iq_idct = s4_saveBlock(img_dct_q_iq_idct, block_dct_q_iq_idct, block_x, block_y);
        }
    }

    if (saveMiddleSteps){
        s1_saveImg("img_dct.raw", img_dct,256);
        s1_saveImg("img_dct_q.raw", img_dct_q,256);
        s1_saveImg("img_dct_q_iq.raw", img_dct_q_iq,256);
        s1_saveImg("img_dct_q_iq_idct.raw", img_dct_q_iq_idct,256);
    }
    return img_dct_q_iq_idct;
}




int main()
{
    cout <<" ====== Quantization Matrix ====== "<<endl;
    float* q_matrix = s4_quantizationMatrix();
    s1_saveImg("q_50_prcnt_matrix.raw", q_matrix,8);

    cout <<" ====== DCT, Q, IQ, IDCT ======= "<<endl;
    float* img = s2_loadImg("lena_256x256.raw", 256);
    float* img_approx = approximate(img,256, true);
    s1_saveImg("img_approx.raw", img_approx,256);

    cout <<" ====== 8bpp ======= "<<endl;
    float* img_approx_8bpp = s4_grayscale8bpp(img_approx,256);
    s1_saveImg("img_approx_8bpp.raw", img_approx_8bpp,256);

    cout <<" ====== Encode/Decode ======= "<<endl;
    float* img_enc = s4_encode(img, 256, false);
    float* img_dec = s4_decode(img_enc, 256, false);
    s1_saveImg("img_enc.raw", img_enc,256);
    s1_saveImg("img_dec.raw", img_dec,256);

    cout << "end of execution" << endl;
    return 0;
}
