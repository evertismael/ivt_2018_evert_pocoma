#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#define M_PI   3.14159265358979323846264338327950288
using namespace std;

/* computes the average in the window */
float s5_computeMean(float* img, int dim){
    float sum = 0.0, mean = 0.0;
    for(int i=0; i<dim*dim;i++){
        sum += *(img + i);
    }
    mean = sum/(dim*dim);
    return mean;
}

/* Downscaled version, average in windows of 8x8*/
float* s5_downScaleTo32x32(float* img, bool quantize){
    float* img_downsaled = new float[32*32]();
    // image is 256x256 so 32x32 blocks.
    float* block = new float[8*8]();
    float value = 0.0;
    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block = s4_getBlock(img, block_x, block_y);
            value = s5_computeMean(block,8);
            if (quantize){ value = round(value/2.0);}
            *(img_downsaled + block_x + block_y*32) = value;
        }
    }
    return img_downsaled;
}

/* Keep only the DC term */
float* s5_keepDCterm(float* img_enc){
    float* img_dc = new float[32*32]();
    // image is 256x256 so 32x32 blocks.
    float* block = new float[8*8]();
    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block = s4_getBlock(img_enc, block_x, block_y);
            *(img_dc + block_x + block_y*32) = *(block);    // DC = first term
        }
    }
    return img_dc;
}

/* Delta encoding */
float* s5_deltaEncoding(float* img_enc_dc, int dim){
    float* delta_dc = new float[dim*dim]();
    float lastDc = 0.0;
    for (int xx = 0; xx<dim*dim; xx++){
        *(delta_dc + xx) = *(img_enc_dc + xx) - lastDc;
        lastDc = *(img_enc_dc + xx);
    }
    return delta_dc;
}

/* Delta decoding */
float* s5_deltaDecoding(float* delta_dc, int dim){
    float* img = new float[dim*dim]();
    float lastDc = 0.0;
    for (int xx = 0; xx<dim*dim; xx++){
        *(img + xx) = *(delta_dc + xx) + lastDc;
        lastDc = *(img + xx);
    }
    return img;
}

/* Save the values of an array to a file.*/
int s5_saveArray(const char* fileName, float* values, int dim, char spacer){
    std::ofstream out(fileName);
    int val = 0;
    for (int xx = 0; xx<dim*dim; xx++){
        val = (int)(*(values + xx));
        out<<std::to_string(val)<<spacer;
    }
    out.close();
    cout<<"Text file created: " << fileName<<endl;
    return 0;
}

/* Reading the values from a text file*/
float* s5_readArray(const char* fileName,int dim,char spacer){
    float *values = new float[dim*dim]();
    string symbol;
    ifstream myfile (fileName);
    float val = 0.0;

    cout<<"Reading text file: " << fileName<<endl;
    if (myfile.is_open()) {
        for (int i=0;i<dim*dim;i++){
            if(getline (myfile,symbol,spacer)){
                val = (float) stoi(symbol);
                *(values + i) = val;
            }
        }
        myfile.close();
    } else cout << "Unable to open file";

    return values;
}

/* Compute energy or vector.*/
float s5_getEnergy(float *vect,int dim){
    float energy = 0.0;
    for(int i=0; i<dim; i++){
        energy += (*(vect+i))*(*(vect+i));
    }
    return energy;
}

/* reorder the 2D array in a zigzag format.
    This function is used in rleEncode function.
*/
float * s5_reorderZigzag(float * block, int dim){
    float* formatted = new float[dim*dim]();
    int x=0,y=0,i=0;
    bool dir_up = false;
    *(formatted) = *(block);
    while(x + y < dim-1 + dim-1){
        if (x==0 && y%2==1 && y!=dim-1){
            y++;
            dir_up = true;
        }else if(x==dim-1 && y%2==1){
            y++;
            dir_up = false;
        }else if (y==0 && x%2==0){
            x++;
            dir_up = false;
        }else if (y==dim-1 && x%2==0){
            x++;
            dir_up = true;
        }
        else if (dir_up){
            y --;
            x ++;
        }else if (!dir_up){
            y ++;
            x --;
        }else{
            cout<<"Error!!"<<endl;
        }
        *(formatted + i + 1) = *(block + x + y*dim);
        i++;
    }
    return formatted;
}

/*  Undo the zigzag format to 2D array.
    This function is used in rleDecode function.
*/
float * s5_undoZigzag(float * formatted, int dim){
    float* block = new float[dim*dim]();
    int x=0,y=0,i=0;
    bool dir_up = false;
    *(block) = *(formatted);
    while(x + y < dim-1 + dim-1){
        if (x==0 && y%2==1 && y!=dim-1){
            y++;
            dir_up = true;
        }else if(x==dim-1 && y%2==1){
            y++;
            dir_up = false;
        }else if (y==0 && x%2==0){
            x++;
            dir_up = false;
        }else if (y==dim-1 && x%2==0){
            x++;
            dir_up = true;
        }
        else if (dir_up){
            y --;
            x ++;
        }else if (!dir_up){
            y ++;
            x --;
        }else{
            cout<<"Error!!"<<endl;
        }
        *(block + x + y*dim) = *(formatted + i + 1);
        i++;
    }
    return block;
}


/* Encodes the rle, and returns the number of symbols,
   and the single array in the format (runlength, amplitud).
*/
int s5_rleEncode(float* block, int dim, float* rle_coeff){
    float* zzv = s5_reorderZigzag(block,dim); // reorder in zigzag format.
    float* amplitud = new float[8*8]();
    float* runlength = new float[8*8]();
    int Nsymb = 0; // Number of symbols
    int Nlast = 0;

    float lastSymbol = *(zzv + 1); // omit the DC term
    for (int i=2; i<dim*dim-1; i++){
        if (*(zzv + i) == lastSymbol){
            Nlast++;
        }else{
            *(amplitud + Nsymb) = lastSymbol;
            *(runlength + Nsymb) = Nlast + 1;
            Nsymb++;
            Nlast = 0;
            lastSymbol = *(zzv+i);
        }
    }
    if (lastSymbol!=*(amplitud + Nsymb - 1)){
        *(amplitud + Nsymb) = lastSymbol;
        *(runlength + Nsymb) = Nlast + 2;
        Nsymb++;
    }
    // merging the arrays (runlength, amplitud);
    for (int i=0; i<Nsymb; i++){
        *(rle_coeff + 2*i) = *(runlength + i);
        *(rle_coeff + 2*i + 1) = *(amplitud + i);
    }
    return Nsymb;
}

/*
    Decodes the rle, and returns the single array in the zigzag format.
*/
float* s5_rleDecode(float* img_acRLE, int dim, bool visualize){
    float* img_ac = new float[dim*dim]();
    int amplitud = 0;
    int runLength = 0;
    int idx = 0;

    float* block_zigzag,* block;
    int NblockCoeff = 0;
    int block_x = 0, block_y = 0;
    do{
        block_zigzag = new float[8*8]();
        NblockCoeff = 0;
        do {
            runLength = *(img_acRLE + 2*idx);
            amplitud = *(img_acRLE + 2*idx + 1);
            for (int i=0; i<runLength; i++){
                *(block_zigzag + 1 + NblockCoeff + i) = amplitud;
            }
            NblockCoeff +=runLength;
            idx++;
        }while(NblockCoeff!=63);

        block = s5_undoZigzag(block_zigzag, 8);
        // put coefficients in correct block.
        img_ac = s4_saveBlock(img_ac, block, block_x, block_y);

        if (block_x ==1 && block_y==0 && visualize){
            cout<<"show debug (decoding) for block (1,0)..."<<endl;
            for (int i=2;i<6;i++){
                cout<<"("<<*(img_acRLE + 2*i)<<","<<*(img_acRLE + 2*i+1)<<")|";
            }
            cout<<endl;
            for (int j=0;j<8;j++){
                for (int i=0;i<8;i++){
                    cout<<*(block+i+j*8)<<" ";
                }
                cout<<endl;
            }
        }
        block_x++;
        if(block_x == int(dim/8.0)){
            block_x = 0;
            block_y++;
        }
    }while(block_x!=0 || block_y!=int(dim/8.0));

    return img_ac;
}

/* Encode , DCT, Q, RLE.
    NOTE: this is the updated version of s4_encode, written in session 4
    returns the total number of symbols.
    img_rle: has the (runlength, amplitud) of the whole image (one block after another).
*/
int s5_encode(float* img, int dim, bool saveMiddleSteps, float* img_acRle, bool visualize){
    float* img_dct = new float[dim*dim]();
    float* img_dct_q = new float[dim*dim]();
    int totalSymbols = 0;

    float* dct_basis = s3_getDctBasis(8);
    float* q_matrix = s4_quantizationMatrix();
    // image is 256x256 so 32x32 blocks.
    float* block = new float[8*8]();
    float* block_dct = new float[8*8]();
    float* block_dct_q = new float[8*8]();
    float* block_acRle = new float[8*8*2]();

    int Nsymbols = 0;
    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block = s4_getBlock(img, block_x, block_y);
            block_dct = s3_DCT_transform(block, dct_basis,8, false);
            block_dct_q = s4_quantize(block_dct, q_matrix, false);
            // Show result for block (1,0)
            if (block_x ==1 && block_y==0 && visualize){
                cout<<"show debug (coding) for block (1,0)..."<<endl;
                for (int j=0;j<8;j++){
                    for (int i=0;i<8;i++){
                        if (*(block_dct_q+i+j*8)==-0){
                            cout<<"0"<<" ";
                        }else{
                            cout<<*(block_dct_q+i+j*8)<<" ";
                        }
                    }
                    cout<<endl;
                }
            }
            Nsymbols = s5_rleEncode(block_dct_q, 8, block_acRle);
            if (saveMiddleSteps){
                img_dct = s4_saveBlock(img_dct, block_dct, block_x, block_y);
                img_dct_q = s4_saveBlock(img_dct_q, block_dct_q, block_x, block_y);
            }
            for (int i = 0; i<Nsymbols; i++){
                *(img_acRle + 2*totalSymbols + 2*i) = *(block_acRle+2*i); // run length
                *(img_acRle + 2*totalSymbols + 2*i+1) = *(block_acRle+2*i+1); // amplitud.
            }
            totalSymbols += Nsymbols;
        }
    }
    if (saveMiddleSteps){
        s1_saveImg("enc_dct.raw", img_dct,256);
        s1_saveImg("enc_dct_q.raw", img_dct_q,256);
    }
    return totalSymbols;
}

/* Decode , invRLE, IQ, IDCT*/

float* s5_decode(float* img_acRLE, float* img_dc, int dim, bool saveMiddleSteps, bool visualize){
    float* dec_iq = new float[dim*dim]();
    float* dec_iq_idct = new float[dim*dim]();

    float* dct_basis = s3_getDctBasis(8);
    float* idct_basis = s3_transpose(dct_basis, 8);

    float* q_matrix = s4_quantizationMatrix();
    // image is 256x256 so 32x32 blocks.
    float* block_enc = new float[8*8]();
    float* block_iq = new float[8*8]();
    float* block_iq_idct = new float[8*8]();

    // recovering the AC coefficients from RLE encoding
    float* img_ac = s5_rleDecode(img_acRLE, 256, visualize);

    for (int block_y = 0; block_y<32; block_y++){
        for (int block_x = 0; block_x<32; block_x++){
            block_enc = s4_getBlock(img_ac, block_x, block_y);
            // recovering the DC coefficient.
            *(block_enc) = *(img_dc + block_x + block_y*32);
            block_iq = s4_inverseQuantize(block_enc, q_matrix);
            block_iq_idct = s3_DCT_transform(block_iq, idct_basis,8, false);
            if (saveMiddleSteps){
                dec_iq = s4_saveBlock(dec_iq, block_iq, block_x, block_y);
            }
            dec_iq_idct = s4_saveBlock(dec_iq_idct, block_iq_idct, block_x, block_y);
        }
    }

    if (saveMiddleSteps){
        s1_saveImg("dec_iq.raw", dec_iq,256);
        s1_saveImg("dec_iq_idct.raw", dec_iq_idct,256);
    }
    return dec_iq_idct;
}

/* Save the rle values of an array to a file.*/
int s5_saveRleFile(const char* fileName, float* values, int dim, char spacer){
    std::ofstream out(fileName);
    int val = 0;
    for (int xx = 0; xx<dim; xx++){
        val = (int)(*(values + xx));
        out<<std::to_string(val)<<spacer;
    }
    out.close();
    cout<<"Text file created: " << fileName<<endl;
    return 0;
}

/* Reading rle the values from a text file*/
float* s5_readRleFile(const char* fileName,int dim,char spacer){
    float *values = new float[dim]();
    string symbol;
    ifstream myfile (fileName);
    float val = 0.0;

    cout<<"Reading text file: " << fileName<<endl;
    if (myfile.is_open()) {
        for (int i=0;i<dim;i++){
            if(getline (myfile,symbol,spacer)){
                val = (float) stoi(symbol);
                *(values + i) = val;
            }
        }
        myfile.close();
    } else cout << "Unable to open file";

    return values;
}
