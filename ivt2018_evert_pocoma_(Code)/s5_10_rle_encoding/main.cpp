#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    cout <<" ====== RLE encoding (test case)======= "<<endl;
    float test_block[] = {-26,-3,-6, 2, 2,-1, 0, 0,
                            0,-2,-4, 1, 1, 0, 0, 0,
                           -3, 1, 5,-1,-1, 0, 0, 0,
                           -3, 1, 2,-1, 0, 0, 0, 0,
                            1, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0};

    float* rle_coeff = new float[8*8*2]();
    int Nsymb = s5_rleEncode(test_block, 8, rle_coeff);
    cout<<"(runLength, Amplitud)|"<<endl;
    for (int i=0; i<Nsymb; i++){
        cout<<"("<<*(rle_coeff + 2*i)<<","<<*(rle_coeff + 2*i+1)<<")|";
    }
    cout<<endl;

    float* img = s2_loadImg("lena_256x256.raw", 256);
    // encoding the DC values.
    float* img_enc = s4_encode(img, 256, false);
    float* img_dc = s5_keepDCterm(img_enc);

    cout <<" ====== AC-terms RLE Encode ======= "<<endl;
    float* img_acRle = new float[256*256*2]();
    int NacSymbols = s5_encode(img,256,false,img_acRle, true);
    s5_saveRleFile("img_ac_rle.txt", img_acRle, NacSymbols*2, ' ');
    cout<<"total symbols after rle:"<<NacSymbols<<endl;


    cout <<" ====== AC-terms RLE Decode ======= "<<endl;
    float* img_dec = s4_grayscale8bpp(s5_decode(img_acRle, img_dc, 256, false,true),256);
    s1_saveImg("img_dec.raw", img_dec,256);

    cout << "end of execution" << endl;
    return 0;
}
