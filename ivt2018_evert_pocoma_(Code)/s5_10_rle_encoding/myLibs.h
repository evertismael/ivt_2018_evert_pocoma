#ifndef MY_LIBS_HEADER
#define MY_LIBS_HEADER

#include <iostream>
#include <fstream>
#include <cstring>
#include <random>
#include <math.h>
#include <sstream> // stringstream

// session 1
int     s1_saveImg(const char* fileName, float *img, int dim);
float*  s1_addUniformNoise(float *img, int dim, float min_val, float max_val);
float*  s1_addGaussianNoise(float *img, int dim, float mean, float stddev);
float   s1_computeMSE(float *img, int dim);

//session 2
float*      s2_loadImg(const char* fileName, int dim);
std::string concatStrNumStr(const char* str1, float num, const char* str2);
float       s2_computePsnr(float *img, float *noised_img, int dim, float max_val);
float*      s2_getBlurGaussianKernel();
float*      convImgWithKernel(float *kernel, float *inImg, int dim, bool borders);

// session 3
float*  s3_getDctBasis(int N);
float*  s3_transpose(float * img_in, int dim);
float*  s3_Mat_multiply(float *A, int Ra, int Ca, float *B, int Rb, int Cb);
float*  s3_DCT_transform(float* img, float*dct_bases,int dim, bool save1D_result);
float*  s3_threshold(float * img,float thr, int dim);

// session 4
float*  s4_quantizationMatrix();
float*  s4_getBlock(float* img, int block_x, int block_y);
float*  s4_saveBlock(float* img, float* block, int block_x, int block_y);
float*  s4_quantize(float* block, float *q, bool clip8bpp);
float*  s4_inverseQuantize(float* block, float *q);
float*  s4_grayscale8bpp(float* img, int dim);
float*  s4_encode(float* img, int dim, bool saveMiddleSteps);
float*  s4_decode(float* img_enc, int dim, bool saveMiddleSteps);

// session 5
float   s5_computeMean(float* img, int dim);
float*  s5_downScaleTo32x32(float* img, bool quantize);
float*  s5_keepDCterm(float* img_enc);
float*  s5_deltaEncoding(float* img_enc_dc, int dim);
float*  s5_deltaDecoding(float* delta_dc, int dim);
int     s5_saveArray(const char* fileName, float* values, int dim, char spacer);
float*  s5_readArray(const char* fileName,int dim,char spacer);
float   s5_getEnergy(float* vect,int dim);
float*  s5_reorderZigzag(float* block, int dim);
float*  s5_undoZigzag(float * formatted, int dim);
int     s5_rleEncode(float* block, int dim, float* rle_coeff);
float*  s5_rleDecode(float* img_acRLE, int dim);
int     s5_encode(float* img, int dim, bool saveMiddleSteps, float* img_acRle, bool visualize);
float*  s5_decode(float* img_acRLE, float* img_dc, int dim, bool saveMiddleSteps, bool visualize);
int     s5_saveRleFile(const char* fileName, float* values, int dim, char spacer);
float*  s5_readRleFile(const char* fileName,int dim,char spacer);

// session 6
std::string s6_dec2bin(unsigned n);
unsigned    s6_bin2dec(std::string n);
std::string s6_golomb(unsigned inVal);
unsigned    s6_golomb(std::string inVal);
std::string s6_golomb_signed(int inValSigned);
int         s6_golomb_signed(std::string inVal);
int         s6_saveString2File(const char* fileName, std::string bitStream);
std::string s6_readStringFromFile(const char* fileName);
float*      s6_mergeArrays(float* array1, int dim1, float* array2, int dim2);
int         s6_separateArrays(float* source, float* array1, int dim1, float* array2, int dim2);
std::string s6_compress(const char* fileName, int dim, bool debug);
float*      s6_decompress(const char* fileName, int dim, bool debug);

#endif
