#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    cout <<" ====== Getting DC terms  ======= "<<endl;
    float* img = s2_loadImg("lena_256x256.raw", 256);
    float* img_enc = s4_encode(img, 256, false);
    float* img_downscaled = s5_downScaleTo32x32(img, false);
    float* img_enc_dc = s5_keepDCterm(img_enc);
    s1_saveImg("img_enc.raw", img_enc,256);
    s1_saveImg("img_downscaled.raw", img_downscaled,32);
    s1_saveImg("img_enc_dc.raw", img_enc_dc,32);

    cout <<" ====== Delta Encoding/Decoding ======= "<<endl;
    float* delta_enc = s5_deltaEncoding(img_enc_dc, 32);
    s1_saveImg("delta_enc.raw", delta_enc,32);
    s5_saveArray("delta_enc.txt", delta_enc, 32,' ');
    float* delta_enc_read = s5_readArray("delta_enc.txt",32,' ');
    float* delta_dec = s5_deltaDecoding(delta_enc_read, 32);
    s1_saveImg("delta_dec.raw", delta_dec,32);

    float energy_no_delta =  s5_getEnergy(img_enc_dc,32*32);
    float energy_with_delta = s5_getEnergy(delta_enc,32*32);
    cout << "Energy before delta encoding: "<<energy_no_delta << endl;
    cout << "Energy after delta encoding: " <<energy_with_delta<< endl;

    cout << "end of execution" << endl;
    return 0;
}
