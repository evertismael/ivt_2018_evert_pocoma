#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
using namespace std;

/*  Reads and copy the values from a image.raw of dimension dim X dim
    to a memory space. Returns the pointer to the memory space.
    */
float* s2_loadImg(const char* fileName, int dim){
    float* img = new float[dim*dim]();
    ifstream myData(fileName, ios::binary);
    char buf[sizeof(float)];
    for(int i=0; i<dim*dim;i++){
        if(myData.read(buf, sizeof(buf))){ // read byte per byte
            memcpy((img + i), buf, sizeof(float));
        }
    }
    cout<<"Opening the image: "<< fileName << endl;
    return img;
}

/* Concatenates two strings and one number, the number goes in between*/
string concatStrNumStr(const char* str1, float num, const char* str2){
    string finalStr = "";
    stringstream stream;
    stream << fixed << setprecision(0) << num;
    finalStr = str1 + stream.str() + str2;
    return finalStr;
}

/* Computes the PSNR between two images given a max value.*/

float s2_computePsnr(float *img, float *noised_img, int dim, float max_val){
    float ise=0, mse = 0, psnr = 0;
    for (int i=0; i < dim*dim ; i++){
        ise +=(*(img + i)- *(noised_img + i))*(*(img + i)- *(noised_img + i));
    }
    mse = ise/(dim*dim);
    psnr = 20*log10(max_val) - 10*log10(mse);
    return psnr;
}

/* returns a 3x3 Gaussian Blur kernel*/
float* s2_getBlurGaussianKernel(){
    float values[] = {1,2,1,
                      2,4,2,
                      1,2,1};
    float* kernel = new float[3*3]();

    for (int i=0; i<3*3; i++){
        *(kernel + i) = (float)values[i]/16;
    }
    return kernel;
}

/* returns the convolution of m1 and kernel
   only handles kernel of dimension 3x3
*/

float* convImgWithKernel(float *kernel, float *inImg, int dim, bool borders){
    float* outImg = new float[dim*dim]();
    int idx = 0, xx=0, yy=0;
    int i_init = 1, j_init = 1;
    float valImg = 0;

    if (borders){
        i_init = 0;
        j_init = 0;
    }


    for (int i = i_init; i<dim-i_init; i++){
        for (int j = j_init; j<dim-j_init; j++){
            *(outImg + i + j*dim) = 0;
            for (int x = -1; x<=1; x++){
                for (int y = -1; y<=1; y++){
                    idx = (x + 1) + (y + 1)*3;
                    // mirror strategy:
                    if (i+x<0){
                        xx = 0;
                    }else if (i+x>dim-1){
                        xx = dim-1;
                    }else{
                        xx = i+x;
                    }

                    if (j+y<0){
                        yy = 0;
                    }else if (j+y>dim-1){
                        yy = dim-1;
                    }else{
                        yy = j+y;
                    }

                    valImg = (*(inImg + (xx) + (yy)*dim));
                    *(outImg + i + j*dim) += (*(kernel + idx))*valImg;
                }
            }
        }
    }
    return outImg;
}
