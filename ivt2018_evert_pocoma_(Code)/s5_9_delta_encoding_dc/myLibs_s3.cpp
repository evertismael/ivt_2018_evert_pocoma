
#include "myLibs.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#define M_PI   3.14159265358979323846264338327950288
using namespace std;

/* Generates the Analysis DCT dictionary*/
float* s3_getDctBasis(int N){
    float *dct_bases = new float[N*N](); // N elements, N basis.
    for (int k=0; k<N; k++){
        for (int i=0; i<N; i++){
            *(dct_bases + i + N*k) = cos (((2*i + 1)*k*M_PI)/(2*(float)N));
            if (k==0){
                *(dct_bases + i + N*k) *= 1.0/sqrt((float)N);
            } else {
                *(dct_bases + i + N*k) *= sqrt(2.0/(float)N);
            }
        }
    }
    return dct_bases;
}

/* Computes the transpose of a matrix of dimension dim*/
float * s3_transpose(float * img_in, int dim){
    float* img_transp = new float[dim*dim];
    for (int y=0;y<dim; y++){
        for (int x=0;x<dim; x++){
            *(img_transp + y + x*dim) = *(img_in + x + y*dim);
        }
    }
    return img_transp;
}

/* Matrix multiplication*/
float * s3_Mat_multiply(float *A, int Ra, int Ca, float *B, int Rb, int Cb){
    if (Ca != Rb){
        throw std::invalid_argument( "Check matrix dimensions" );
    }
    float *C = new float[Ra*Cb]();
    for (int row=0;row<Ra; row++){
        for (int col=0; col<Cb; col++){
            float sum = 0.0;
            for (int z=0;z<Ca; z++){
                sum += (*(A + z + row*Ca)) * (*(B + col + z*Cb));
            }
            *(C + col + row*Cb) = sum;
        }
    }
    return C;
}

/* Compute the DCT transform */
float* s3_DCT_transform(float* img, float*dct_bases,int dim, bool save1D_result){
    float* dct1D = s3_Mat_multiply(dct_bases,dim,dim,img,dim,dim);
    if (save1D_result){
        s1_saveImg("img_dct_1D_(partial_result).raw", dct1D, dim);
    }
    float* dct_bases_t = s3_transpose(dct_bases,dim);
    float* dct2D = s3_Mat_multiply(dct1D,dim,dim,dct_bases_t,dim,dim);
    return dct2D;
}
/* Threshold operation*/
float * s3_threshold(float * img,float thr, int dim){
    float* img_thresh = new float[dim*dim]();
    for (int i=0;i<dim*dim; i++){
        if ( abs(*(img + i)) <= thr){
            *(img_thresh + i) = 0;
        }else{
            *(img_thresh + i) = *(img + i);
        }
    }
    return img_thresh;
}
