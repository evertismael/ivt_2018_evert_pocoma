#include <iostream>
/* Including my libs */
#include "myLibs.h"

using namespace std;

int main()
{
    cout <<" ====== Exponential Golomb (unsigned -> string) ======= "<<endl;
    for (int i=0; i<=10;i++){
        string glmb_code = s6_golomb((unsigned)i);
        cout<< i <<" \t -> \t"<<glmb_code<< endl;
    }

    cout <<" ====== Exponential Golomb (string -> unsigned) ======= "<<endl;
    for (int i=0; i<=10;i++){
        string glmb_code = s6_golomb((unsigned)i);
        cout<< glmb_code<<"\t -> \t"<<s6_golomb(glmb_code)<< endl;
    }

    cout <<" ====== Exponential Golomb (signed -> string) ======= "<<endl;
    for (int i=80; i<=85;i++){
        string glmb_code_pos = s6_golomb_signed(i);
        string glmb_code_neg = s6_golomb_signed(-i);
        cout<< i <<" \t -> \t"<<glmb_code_pos<< endl;
        cout<< -i <<" \t -> \t"<<glmb_code_neg<< endl;
    }

    cout <<" ====== Exponential Golomb (string -> signed) ======= "<<endl;
    for (int i=80; i<=85;i++){
        string glmb_code_pos = s6_golomb_signed(i);
        string glmb_code_neg = s6_golomb_signed(-i);
        cout<< glmb_code_pos<<"\t -> \t"<<s6_golomb_signed(glmb_code_pos)<< endl;
        cout<< glmb_code_neg<<"\t -> \t"<<s6_golomb_signed(glmb_code_neg)<< endl;
    }
    cout << "end of execution" << endl;
    return 0;
}
