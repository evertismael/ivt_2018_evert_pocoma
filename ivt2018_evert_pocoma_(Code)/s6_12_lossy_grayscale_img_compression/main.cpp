#include <iostream>
#include <string>
/* Including my libs */
#include "myLibs.h"
using namespace std;

int main()
{
    cout <<" ====== Compress image ======= "<<endl;
    std::string compressedStream = s6_compress("lena_256x256.raw", 256, true);
    s6_saveString2File("lena_compressed.txt", compressedStream);

    cout <<" ====== Decompress image ======= "<<endl;
    float* img_recov = s4_grayscale8bpp(s6_decompress("lena_compressed.txt", 256, true),256);
    s1_saveImg("lena_decompressed.raw", img_recov, 256);

    cout<< "end of execution" << endl;

    return 0;
}
